const burger = document.querySelector("#headerburger");
const headermenu = document.querySelector("#headermenu");
const closeheadermenu = document.querySelector("#closemenu");
const openElems = document.getElementsByClassName("open-outside-element");
const activeElems = document.getElementsByClassName("active-outside-element");
const searchselect = document.querySelector("#searchSelect");
const searchselectopts = document.querySelector("#searchSelectOptions");
const pageScrollUp = document.querySelector(".page__scrollUp");
const catalogFilter = document.querySelector(".catalog__filter");
const catalogFilterButton = document.querySelector(".catalog__filter-button");
const closeFilter = document.querySelector(".catalog__filter-back");
const closeModalButton = document.querySelector(".modal-form-resetbutton");
const formOptionButton = document.querySelector(".form-add-option-button");
const formOptionBlock = document.querySelector(".form-options-block");
var myModal;
const fileInput = document.getElementById('file-input');


burger.addEventListener("click", burgerHandler);
closeheadermenu.addEventListener("click", closeHandler);
if (searchselect) searchselect.addEventListener("click", toggleSearch);
if (catalogFilterButton) catalogFilterButton.addEventListener("click", toggleFiltr);
if (closeFilter) closeFilter.addEventListener("click", toggleFiltr);
window.addEventListener('click', closeElems);
document.addEventListener("DOMContentLoaded", scrollPage);
if (formOptionButton) formOptionButton.addEventListener('click', addOptionBlock);

if (fileInput) {
  fileInput.addEventListener('change', function() {
    var reader = new FileReader();
    reader.onload = function() {
      var output = document.getElementById('file-preview');
      output.src = reader.result;
      output.style.display = 'block';
    }
    reader.readAsDataURL(event.target.files[0]);
  });
}


if (closeModalButton)  { 
  myModal = new HystModal({
    linkAttributeName: "data-hystmodal",
  });
}

function closeModal(e) {
  e.preventDefault();
  myModal.close();
}

function burgerHandler(e) {
  e.preventDefault();
  headermenu.classList.add("open");
  fixBody();
  if (window.innerWidth < 768) {
    atalogFilterButton.classList.remove('active');
    catalogFilter.classList.remove('open');
  }
}

function closeHandler(e) {
  e.preventDefault();
  headermenu.classList.remove("open");
  unfixBody();
}

function toggleLang(element) {
  if (element.classList.contains('open')) {
    element.classList.remove('open');
  }
  else {
    element.classList.add('open');
  }
}

function closeElems(e) {
  const target = e.target;
  if (!target.closest('.outside-close-element')) { 
    for (let elem of openElems) {
      elem.classList.remove('open');
    }
    for (let elem of activeElems) {
      elem.classList.remove('active');
    }
  }
}

function closeSearchOptions() {
  searchselect.classList.add('active');
  searchselectopts.classList.add('open');
}

function toggleSearch() {
  if (searchselect.classList.contains('active')) {
    searchselect.classList.remove('active');
    searchselectopts.classList.remove('open');
  }
  else {
    closeSearchOptions();
  }
}

function setSearch(e) {
  var label = e.target.dataset.label;
  if (label !== undefined) { 
    var selspan = searchselect.querySelector('.catalog__sortlist-select-value');
    selspan.innerText = label;
    toggleSearch();
  }
}

function scrollPage() {
  window.addEventListener("scroll", function () {
    if (window.scrollY > 300) {
      pageScrollUp.style.display = "block";
    } else {
      pageScrollUp.style.display = "none";
    }
  }); 
  if (pageScrollUp) { 
    pageScrollUp.addEventListener("click", function (event) {
      event.preventDefault();
      window.scrollTo({ top: 0, behavior: "smooth" });
    });
  }
}

function activeFilterInput(el) {
  var val = el.value.trim();
  var parentel = el.closest('.catalog__filter-label');
  var eloptions = parentel.nextElementSibling;
  if (val !== '' && eloptions !== null) {
    eloptions.classList.add('active');
  }
  else {
    eloptions.classList.remove('active');
  }
}

function setFilterValue(el) {
  var val = el.dataset.text;
  var parentel = el.closest('.filter-input-options');
  var parentinput = parentel.closest('.catalog__filter-block').firstElementChild;
  var inputel = parentinput.firstElementChild;
  inputel.value = val;
  parentel.classList.remove('active');  
}

function fixBody() {
  document.body.style.position = 'fixed';
  document.body.style.top = `-${window.scrollY}px`;
  document.body.style.left = '0px';
  document.body.style.right = '0px';
}
function unfixBody() {
  const scrollY = document.body.style.top;
  document.body.style.position = '';
  document.body.style.top = '';
  window.scrollTo(0, parseInt(scrollY || '0') * -1);
}
function toggleFiltr() {
  if (window.innerWidth < 768) {
    if (!catalogFilter.classList.contains('open')) {
      fixBody();
    } else {
      unfixBody();
    }
  }
  catalogFilterButton.classList.toggle('active');
  catalogFilter.classList.toggle('open');
}

function addOptionBlock(e) { 
  e.preventDefault();
  var txt = document.createElement('textarea');
  txt.className = "form-textarea options-textarea";
  txt.name = "options";
  txt.placeholder = "Например, крутящий момент 100000"
  formOptionBlock.append(txt);
}
